import datetime
from unittest import mock

from django.test import Client
from django.urls import reverse
import django.utils.timezone
import looper.models
import looper.signals
from looper.tests import AbstractLooperTestCase

from blender_fund_main.models import Membership, MembershipLevel, Activity


@mock.patch('looper.gateways.BraintreeGateway.transact_sale', return_value='mocked-transaction-id')
class ActivityMembershipTestCase(AbstractLooperTestCase):
    fixtures = ['default_site', 'devfund', 'testuser', 'systemuser']

    def test_activity_membership_lifecycle(self, mock_transact_sale):
        """Create, renew and delete a subscription."""

        self.assertEqual(0, Activity.objects.count())
        sub = self.create_active_subscription()
        sub.intervals_elapsed = 1
        sub.save()
        self.assertEqual('active', sub.membership.status)

        # An Activity is generated on subscription activation
        self.assertEqual(1, Activity.objects.count())

        activity = Activity.objects.first()
        # Membership is attached to the activity
        self.assertEqual(sub.membership, activity.membership)
        self.assertEqual(Activity.Categories.MEMBERSHIP_START.value, activity.category)

        # An Activity is generated on subscription renewal
        start_time = django.utils.timezone.now()
        old_next_payment = start_time - datetime.timedelta(hours=1)
        sub.next_payment = old_next_payment
        sub.save()
        order = sub.generate_order()
        trans = order.generate_transaction(ip_address='fe80::5ad5:4eaf:feb0:4747')
        mock_transact_sale.assert_not_called()
        self.assertTrue(trans.charge())
        order.refresh_from_db()
        trans.refresh_from_db()
        looper.signals.automatic_payment_succesful.send(sender=order, transaction=trans)
        self.assertEqual(2, Activity.objects.count())
        activity = Activity.objects.first()
        self.assertEqual(Activity.Categories.MEMBERSHIP_RENEW.value, activity.category)

        # When deleting a membership, activity.membership is set to NULL
        sub.refresh_from_db()
        sub.membership.delete()
        self.assertEqual(0, Membership.objects.count())
        activity.refresh_from_db()
        self.assertEqual(None, activity.membership)


class ActivityTestCase(AbstractLooperTestCase):
    fixtures = ['default_site', 'devfund', 'testuser', 'systemuser']

    def test_activity_announcement(self):
        act = Activity.objects.create(title='Annuäl Report')
        self.assertEqual(Activity.Categories.ANNOUNCEMENT.value, act.category)

    def test_activity_dev_grant(self):
        act = Activity.objects.create(title='New Dev Grant', category=Activity.Categories.DEV_GRANT)
        self.assertEqual(Activity.Categories.DEV_GRANT.value, act.category)

    def test_activity_list_view(self):
        act = Activity.objects.create(title='Annual Report')
        client = Client()
        response = client.get(reverse('activity-list'))
        self.assertEqual(200, response.status_code)
        self.assertEqual(act, response.context['page_obj'][0])


class ActivityDisplayTitleTestCase(AbstractLooperTestCase):
    fixtures = ['default_site', 'devfund', 'testuser', 'systemuser']

    def test_activity_display_title_membership(self):
        sub = self.create_active_subscription()
        act = Activity.objects.first()
        self.assertEqual('Harry de Bøker started a Gold membership', act.display_title)
        act.category = Activity.Categories.MEMBERSHIP_RENEW
        act.save()
        self.assertEqual('Harry de Bøker renewed their Gold membership', act.display_title)

        sub.membership.display_name = ''
        sub.membership.save()
        act.refresh_from_db()
        self.assertEqual('Someone renewed their Gold membership', act.display_title)

        sub.membership.level = MembershipLevel.objects.get(name='Bronze')
        sub.membership.display_name = 'Harry'
        sub.membership.save()
        act.refresh_from_db()
        self.assertEqual('Someone renewed their Bronze membership', act.display_title)

    def test_activity_display_title_announcement(self):
        act = Activity.objects.create(title='Annuäl Report')
        self.assertEqual('Annuäl Report', act.display_title)
        act.title = ''
        act.save()
        self.assertEqual('New Announcement', act.display_title)

    def test_activity_display_title_dev_grant(self):
        act = Activity.objects.create(title='New Dev Grant', category=Activity.Categories.DEV_GRANT)
        self.assertEqual('New Dev Grant', act.display_title)
        act.title = ''
        act.save()
        self.assertEqual('New Development Grant', act.display_title)
