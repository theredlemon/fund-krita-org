from django.conf import settings
from django.contrib.auth import get_user_model
from django.test import override_settings
from django.urls import reverse

from looper.tests import AbstractLooperTestCase

User = get_user_model()

production_storage = settings.STATICFILES_STORAGE


# This test needs to use the static file storage we also use in production,
# regardless of what the superclass overrides. There was an issue finding
# the image file, which went unseen because we test with a static file
# storage class that doesn't require you to run 'manage.py collectstatic'.
@override_settings(
    STATICFILES_STORAGE=production_storage,
)
class TotalIncomeTest(AbstractLooperTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        # Run the collectstatic command, as that's needed for the production
        # storage class to work.
        from django.contrib.staticfiles.management.commands import collectstatic
        cmd = collectstatic.Command()

        # This mimicks the relevant part of BaseCommand.run_from_argv()
        parser = cmd.create_parser('python', 'collectstatic')
        options = parser.parse_args(['--no-input'])
        cmd_options = vars(options)
        cmd.handle(**cmd_options)

    def test_get_receipt_pdf(self):
        subs = self.create_active_subscription()
        order_id = subs.latest_order().id
        self.client.force_login(self.user)
        resp = self.client.get(reverse('settings_receipt_pdf',
                                       kwargs={'order_id': order_id}))
        self.assertEqual(200, resp.status_code)
        self.assertEqual(b'%PDF-', resp.content[:5])

        # Check that the image is there.
        self.assertIn(b'/Subtype /Image', resp.content)
        self.assertIn(b'/Height 125', resp.content)
        self.assertIn(b'/Width 601', resp.content)

    def test_get_receipt_pdf_donation(self):
        subs = self.create_active_subscription()
        product = subs.plan.product
        product.description = 'Product description here'
        product.save(update_fields={'description'})
        order_id = subs.latest_order().id
        self.client.force_login(self.user)
        resp = self.client.get(reverse('settings_receipt_pdf',
                                       kwargs={'order_id': order_id}))
        self.assertEqual(200, resp.status_code)

        # Check that "donation" is there.
        self.assertIn(b'Donation Receipt', resp.content)
