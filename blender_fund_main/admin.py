from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from django.urls import reverse
from django.utils.html import format_html
from django.utils.translation import gettext_lazy as _

from looper import decorators
from looper.admin import subscription_link, create_admin_fk_link, plan_link, EditableWhenNewMixin, \
    copy_to_clipboard_link, USER_SEARCH_FIELDS
import looper.forms
import looper.models
from . import models, forms, page_models

admin.site.site_header = 'Krita Development Fund'
admin.site.index_title = 'Fund Membership and Subscription Administration'
admin.site.enable_nav_sidebar = False

membership_link = create_admin_fk_link(
    'membership', 'Membership', 'admin:blender_fund_main_membership_change'
)
user_link = create_admin_fk_link('user', 'user', 'admin:auth_user_change')


@decorators.short_description('Links for Customers')
def subscribe_memlev_link(memlev: models.MembershipLevel) -> str:
    """Show the link to pay for & create a subscription, can be sent to users."""
    if not memlev or not memlev.pk:
        return ''

    return copy_to_clipboard_link('looper:checkout_new',
                                  kwargs={'plan_id': memlev.plan_id},
                                  link_text='Pay & Subscribe',
                                  title='Send this to customers')


@admin.register(models.MembershipLevel)
class MembershipLevelAdmin(admin.ModelAdmin):
    model = models.MembershipLevel
    list_display = ('name', 'category', 'visible_attributes', 'order', plan_link, 'is_popular',
                    subscribe_memlev_link)
    list_editable = ('category', 'visible_attributes', 'order', 'is_popular')
    list_filter = ('category',)
    search_fields = ('category', 'name')


@decorators.short_description('Last Order Receipt')
def last_order_pdf_link(membership: models.Membership) -> str:
    subs: looper.models.Subscription = membership.subscription
    if not subs:
        return '-'
    order = subs.latest_order()
    if not order:
        return '-'
    if order.status != 'paid':
        return '-'

    pdf_url = reverse('settings_receipt_pdf', kwargs={'order_id': order.id})
    return format_html('<a href="{}" target="_blank">PDF for order #{}</a>', pdf_url, order.pk)


@admin.register(models.Membership)
class MembershipAdmin(EditableWhenNewMixin, admin.ModelAdmin):
    model = models.Membership
    list_display = ('id', 'user', 'display_name', 'level', 'status_icon', 'is_managed',
                    'is_private', subscription_link, last_order_pdf_link)
    list_display_links = ('id', 'user', 'display_name')
    list_filter = ('level', 'is_managed', 'status', 'is_private')
    search_fields = ('id', 'user__username', 'user__email',
                     'user__customer__full_name', 'user__customer__billing_email',
                     'display_name', 'url')

    raw_id_fields = ['user']
    readonly_fields = [subscription_link, 'is_managed', 'created_at', 'updated_at', 'granted_badge']
    editable_when_new = {subscription_link}
    fieldsets = [
        (None, {
            'fields': ['user', subscription_link, 'is_managed', 'status', 'level', 'granted_badge',
                       'created_at', 'updated_at'],
        }),
        ('Attributes', {
            'fields': ['display_name', 'description', 'url', 'logo', 'is_private']
        }),
    ]
    add_fieldsets = [
        (None, {
            'fields': ['user', 'status', 'level'],
        }),
        ('Subscription Parameters', {
            'fields': ['currency', 'price', 'interval_unit', 'interval_length'],
        }),
        ('Attributes', {
            'fields': ['display_name', 'description', 'url', 'logo', 'is_private'],
        }),
    ]

    def get_form(self, request, obj=None, **kwargs):
        if obj is None:
            kwargs['form'] = forms.MembershipAdminAddForm
        form = super().get_form(request, obj, **kwargs)
        return form

    def get_fieldsets(self, request, obj=None):
        if obj is None:
            return self.add_fieldsets
        return super().get_fieldsets(request, obj)

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        if add:
            context['title'] = 'Add Managed Membership'
        return super().render_change_form(request, context, add, change, form_url, obj)

    def get_changeform_initial_data(self, request) -> dict:
        return {
            **super().get_changeform_initial_data(request),
            'status': 'active',
            'user': 1,
        }

    @decorators.short_description('Is active')
    def status_icon(self, membership: models.Membership):
        from django.templatetags.static import static

        try:
            icon = {'active': 'yes', 'inactive': 'no'}[membership.status]
        except KeyError:
            icon = 'unknown'

        icon_url = static(f'admin/img/icon-{icon}.svg')
        return format_html('<img src="{}" alt="{}" title="{}">',
                           icon_url, membership.status, membership.status)


admin.site.unregister(User)
admin.site.unregister(looper.models.Subscription)


@admin.register(looper.models.Subscription)
class SubscriptionAdmin(looper.admin.SubscriptionAdmin):
    """Override Subscription admin, adding a link to the membership."""

    readonly_fields = looper.admin.SubscriptionAdmin.readonly_fields + [membership_link]
    # Add `membership_link` to the main section of Subscription details
    fieldsets = [
        (
            name,
            {
                'fields': _set['fields'] + [membership_link]
            } if name is None else _set
        ) for name, _set in looper.admin.SubscriptionAdmin.fieldsets
    ]


@admin.register(User)
class UserAdmin(BaseUserAdmin):
    list_display = ('username', 'email', 'user_memberships_link', 'is_staff', 'is_superuser')

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'password1', 'password2', 'email'),
        }),
    )
    form = looper.forms.AdminUserChangeForm
    add_form = looper.forms.AdminUserCreationForm

    inlines = (looper.admin.CustomerInline,
               looper.admin.GatewayCustomerIdInline,
               looper.admin.PaymentMethodInline,
               looper.admin.AddressInline)

    fieldsets = (
        (None, {'fields': ('username', 'password', 'email', 'user_memberships_link',
                           'user_subscriptions_link')}),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
            'classes': ('collapse',),
        }),
        (_('Important dates'), {
            'fields': ('last_login', 'date_joined'),
            'classes': ('collapse',),
        }),
    )

    def get_inline_instances(self, request, obj=None):
        """Show inlines only for already-existing users."""
        if obj is None:
            return []
        return super().get_inline_instances(request, obj=obj)

    def get_readonly_fields(self, request, obj=None):
        return [*super().get_readonly_fields(request, obj=obj),
                'user_memberships_link', 'user_subscriptions_link']

    @decorators.short_description('Memberships')
    def user_memberships_link(self, user: models.User) -> str:
        from django.urls import reverse
        from urllib.parse import urljoin

        count = user.memberships.count()
        if count == 0:
            return '-'
        if count == 1:
            admin_link = reverse('admin:blender_fund_main_membership_change',
                                 kwargs={'object_id': user.memberships.first().pk})
            label = '1 membership'
        else:
            admin_link = urljoin(reverse('admin:blender_fund_main_membership_changelist'),
                                 f'?user_id={user.pk}')
            label = f'{count} memberships'
        return format_html('<a href="{}">{}</a>', admin_link, label)

    @decorators.short_description('Subscriptions')
    def user_subscriptions_link(self, user: models.User) -> str:
        from django.urls import reverse
        from urllib.parse import urljoin

        count = user.subscription_set.count()
        if count == 0:
            return '-'
        if count == 1:
            admin_link = reverse('admin:looper_subscription_change',
                                 kwargs={'object_id': user.memberships.first().pk})
            label = '1 subscription'
        else:
            admin_link = urljoin(reverse('admin:looper_subscription_changelist'),
                                 f'?user_id={user.pk}')
            label = f'{count} subscriptions'
        return format_html('<a href="{}">{}</a>', admin_link, label)


@admin.register(page_models.HomePage)
class UserAdmin(admin.ModelAdmin):
    list_display = ('body', 'tagline', 'description_text')


def can_login_as(request, target_user: User) -> bool:
    """Determine who can use the 'login as' feature.

    See https://github.com/skorokithakis/django-loginas
    """

    return request.user.is_staff and request.user.has_perm('auth.can_loginas')


@admin.register(models.Activity)
class ActivityAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'category', 'created_at']
    autocomplete_fields = ['membership']
