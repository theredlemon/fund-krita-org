import random
from django.core.management import BaseCommand
from blender_fund_main.models import MembershipLevel
from blender_fund_main import forms


class Command(BaseCommand):
    help = 'Creates 5 fake membership, with semi-random attributes.'

    def handle(self, *args, **options):
        membership_levels = MembershipLevel.objects.all()
        for i in range(5):
            form = forms.MembershipAdminAddForm(
                {
                    'user': '1',
                    'status': 'active',
                    'level': str(random.choice(membership_levels).pk),
                    'interval_unit': 'month',
                    'interval_length': '3',
                    'currency': 'USD',
                    'price': random.choice(['3000', '6000', '8000']),
                    'display_name': '',
                    'description': '',
                }
            )
            form.full_clean()
            form.save()
        self.stdout.write(self.style.SUCCESS('Fake memberships created.'))
