Hi Ton,

{{ user.customer.full_name|default:user.email }} has a {{ membership.level.name }} membership that just
passed its 'next payment' date.

Some Dev Fund admin links:
    - The Membership: {{ memb_admin_url }}
    - The Subscription: {{ subs_admin_url }}

Cheers,

The Dev Fund cronjob
