from django.forms import ModelForm, Textarea

from . import models


class NoteForm(ModelForm):
    class Meta:
        model = models.Note
        fields = ['note']
        widgets = {
            'note': Textarea(attrs={'cols': 20, 'rows': 3})
        }
