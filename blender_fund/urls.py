from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from wagtail.admin import urls as wagtailadmin_urls
from wagtail.documents import urls as wagtaildocs_urls
from wagtail.core import urls as wagtail_urls
from django.contrib.auth.views import redirect_to_login
from django.urls import reverse
from django.http import HttpResponse

from blender_fund_main.views import errors

handler500 = errors.error_handler_500

def redirect_to_my_auth(request):
    return redirect_to_login(reverse('wagtailadmin_home'))


urlpatterns = [
    path('accounts/', include('allauth.urls')),
    path('admin/doc/', include('django.contrib.admindocs.urls')),
    path('admin/', include('loginas.urls')),
    path('admin/notes/', include('blender_notes.urls')),
    path('admin/', admin.site.urls),
    path('error/500', errors.test_error_500),

    path('', include('blender_fund_main.urls')),
    path('', include('looper.urls')),

    # Wagtail URLs
    path('cms/login/', redirect_to_my_auth, name='wagtailadmin_login'),
    path('cms/', include(wagtailadmin_urls)),
    path('documents/', include(wagtaildocs_urls)),
    path('', include(wagtail_urls)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + \
    static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
