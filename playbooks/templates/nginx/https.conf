# This will be updated by certbot.
map $remote_addr $allowed {
    default 0;
    127.0.0.1 1; # For local connections
{% if allowed_ips is defined %}{% for ip in allowed_ips %}
    {{ ip }} 1;
{% endfor %}{% endif %}
}

server {
    server_name {{ domain }};
    # Avoid DisallowedHost "Invalid HTTP_HOST header" errors
    if ($host !~* ^({{ domain }})$) {
        return 444;
    }

{% if env != 'production' %}{% for ip in allowed_ips %}
    allow {{ ip }};
{% endfor %}
    deny all;
{% endif %}
    access_log /var/log/nginx/{{ domain }}-access.log;
    error_log /var/log/nginx/{{ domain }}-error.log;

    #location /robots.txt {
    #    add_header Content-Type text/plain;
    #    return 200 "User-agent: *\nDisallow: /\n";
    #}

    location / {
        set $maintenance 0;
        if (-f {{ dir.errors }}/maintenance_on) {
           set $maintenance 1;
        }
        if ($allowed != 1) {
           set $maintenance "${maintenance}1";
        }
        if ($maintenance = 11) {
           return 503;
        }
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;

        proxy_pass http://{{ host }}/;
        proxy_connect_timeout 600s;
        proxy_read_timeout 600s;
        proxy_send_timeout 600s;
        proxy_ssl_name $host;
        proxy_ssl_server_name on;
        send_timeout 600s;
    }

    error_page 502 /503-service-unavailable.html;
    error_page 503 /503-service-unavailable.html;
    location /503-service-unavailable.html {
        root {{ dir.errors }};
        internal;
    }

    # Some other security related headers
    # NOTE: Avoiding use of includeSubdomains in case "blender.org" alias causes it
    # to match every subdomain of blender.org, at least until we are ready -- Dan
    add_header Strict-Transport-Security "max-age=63072000; preload";
    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-Xss-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";
{% if "127.0.0.1" in host %}
    location /media/  {
        # must end with a slash in order to work
        alias {{ dir.media | regex_replace('\\/*$', '/') }};
    }
    location /static/  {
        # must end with a slash in order to work
        alias {{ dir.static | regex_replace('\\/*$', '/') }};
    }
{% endif %}
}
